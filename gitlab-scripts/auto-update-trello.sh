#!/bin/sh

#get branch name, check card id

branchName = $CI_COMMIT_REF_NAME

IFS='- ' read -r -a array <<< branchName

cardSubURL = ${array[1]}

nextListId = "5b6f6d1df1e6396122b77e23"

#check commit title, if it has start use TODO -> CurrentlyDoing
#if its a merge request, use CurrentlyDoing->Done

moveCard = "curl -X PUT -d https://api.trello.com/1/cards/" + cardSubURL + "?idList=" + nextListId + "&key=" + $TRELLO_KEY + "&token=" + $TRELLO_TOKEN

echo moveCard
eval moveCard